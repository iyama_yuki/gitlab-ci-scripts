#!/bin/bash
chmod u+x scripts/version_up.sh

echo "release"
merge_branch_name=$(git log --pretty=oneline --abbrev-commit --merges -n 1 | awk '{print $4}' | sed 's/'\''//g')
echo $merge_branch_name

if [ $merge_branch_name = "develop" ]; then
  echo 1.0.0 | scripts/version_up.sh major
else
  echo 1.0.0 | scripts/version_up.sh bugfix
fi