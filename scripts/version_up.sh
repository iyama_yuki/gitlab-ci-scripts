#!/bin/sh
version=`awk '{print $1}' < /dev/stdin`
command=$1
major=`echo $version | awk -F '.' '{print $1}'`
minor=`echo $version | awk -F '.' '{print $2}'`
bugfix=`echo $version | awk -F '.' '{print $3}'`

if [ -z "$version" ]; then
  echo "USAGE: echo 1.0.0 | version_up.sh [major|minor|bugfix]"
  exit 1
fi

if [ "$command" = '' ]; then
    bugfix=`expr $bugfix + 1`
elif [ "$command" = 'major' ]; then
  major=`expr $major + 1`
  minor=0
  bugfix=0
elif [ "$command" = 'minor' ]; then
  minor=`expr $minor + 1`
  bugfix=0
elif [ "$command" = 'bugfix' ]; then
  bugfix=`expr $bugfix + 1`
fi

echo ${major}.${minor}.${bugfix}
