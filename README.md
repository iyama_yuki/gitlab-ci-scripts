# gitlab_ci_scripts

gitlab_ci_scriptsは下記ブランチモデルで使用する自動化スクリプトです。

![](./docs/branchmodel.jpg)

## Feature
### 自動タグ更新
masterブランチにマージした際に、マージされたブランチに合わせてタグを更新します。
```bash:
Merge branch 'develop' into 'master'
1.0.0 -> 2.0.0
Merge branch 'hotfix' into 'master'
1.0.0 -> 1.0.1
```

## Usage

